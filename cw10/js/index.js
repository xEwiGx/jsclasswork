    window.addEventListener('load', function OnWindowLoaded() {
        const inputBtn = [
            'c', '%', '/',
            '7', '8', '9', '*',
            '4', '5', '6', '-',
            '1', '2', '3', '+',
            '0', '.', '='
        ];
        const calc = document.getElementById('calc');
        const textArea = document.getElementById('inputNum');
        // для каждого элемента массива inputBtn в div #calc под textarea будут добавлены div.btn
        inputBtn.forEach(function (sign) {
            var signElement = document.createElement('div');
            signElement.className = 'btn';
            signElement.innerHTML = sign;
            calc.appendChild(signElement);
        });
        // Для всех кнопок в контейнере 
        document.querySelectorAll('#container .btn').forEach(function (button) {
            // Добавляем обрабутку по клику с функцией соответсвующей
            button.addEventListener('click', onButtonClick);
        });
        // добавлем функции для обработчика
        function onButtonClick(e) {
            // Если нажата кнопка "с", то возвращаем 0 в текстовое поле
            if (e.target.innerHTML === 'c') {
                textArea.innerHTML = '0';
            } else if (e.target.innerHTML === '=') {
                textArea.innerHTML = eval(textArea.innerHTML); // Метод eval() выполняет JavaScript код, представленный строкой.
            } else if (textArea.innerHTML === '0') {
                // Если textarea 0 то при нажатии замняем на значение кнопки
                textArea.innerHTML = e.target.innerHTML;
            } else {
                // если не 0 то прибавляем к существующему значению новое нажатое значение
                textArea.innerHTML += e.target.innerHTML;
            }
        }
    });