// var name = 'Alexandr',
//     surname = 'Petrenko';

// function user() {
//     console.log(this);
// }
// user();
// const userOne = {
//     name: 'Ivan',
//     surname: 'Ivanov',
//     age: 95,
//     hello: function (phone) {
//         console.log(`Hello my name is ${this.name} ${this.surname} My phone number ${phone}`);
//     }

// };
// userOne.hello.bind(window)();
// const userTwo = {
//     name: 'Oleg',
//     surname: 'Burda',
//     age: 12
// };
// userOne.hello.bind(userTwo)();

// userOne.hello.call(userTwo, ['+380639218337', '+380634538337']);

//2 ==========================================================

Array.prototype.mul = function (num) {
    return this.map(element => {
        return element * num;
    });
};
const arr = [5, 15, 20];
console.log(arr.mul(20));