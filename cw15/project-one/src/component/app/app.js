import React from 'react'
import '../../css/style.min.css'
import NavMenu from '../navmenu/navmenu'
import Main from '../main/main'
import Footer from '../footer/footer'

const App = () => {
    return (
        <div className="container">
            <NavMenu />
            <Main />
            <Footer />
        </div>
    )
}

export default App