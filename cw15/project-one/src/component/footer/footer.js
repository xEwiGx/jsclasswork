import React from 'react'

const Footer = () => {
    return (
        <div className="footer">
            <p className="footer_text">Copyright by phototime - all right reserved</p>
        </div>
    )
}

export default Footer