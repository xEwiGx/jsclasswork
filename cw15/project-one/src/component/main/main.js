import React from 'react'
import Bigimg from './bigimg/bigimg'
import Content from './content/content'

const Main = () => {
    return (
        <div className="main">
            <Bigimg />
            <Content />
        </div>

    )
}

export default Main