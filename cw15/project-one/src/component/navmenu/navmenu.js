import React from 'react'


const NavMenu = () => {
    return (

        <div className="header">
            <ul className="header_nav">
                <li><a href="#" className="header_nav_item">home</a></li>
                <li><a href="#" className="header_nav_item">photoapp</a></li>
                <li><a href="#" className="header_nav_item">design</a></li>
                <li><a href="#" className="header_nav_item">download</a></li>
            </ul>
        </div>
    )
}

export default NavMenu